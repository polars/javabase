#构建javabase，使用jlink打包最小运行时
FROM ubuntu:18.04

ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

RUN apt-get update \
    && apt-get install -y --no-install-recommends curl net-tools iputils-ping ca-certificates fontconfig locales tzdata \
    && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen en_US.UTF-8 \
    && ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    find /var/lib/apt/lists -type f -delete && \
    find /var/cache -type f -delete \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/* \
    && rm -rf /tmp/* \
    && apt-get purge \
    && apt-get autoremove \
    && apt-get clean \
    && groupadd app \
    && useradd -g app -s /bin/bash -m app \
    && chmod u+s /bin/* \
    && chown -R app:app /opt
