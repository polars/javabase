FROM registry.cn-hangzhou.aliyuncs.com/ecf/javabase:11

ENV JAVA_VERSION jdk-11.0.6+10

ENV JAVA_HOME=/opt/jdk \
    PATH="/opt/jdk/bin:$PATH"
CMD ["jshell"]
