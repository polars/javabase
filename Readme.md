## jdk_11x: 跨平台镜像支持
在jdk_11x目录下执行
构建ubuntu镜像
```bash
docker buildx build -t registry.cn-hangzhou.aliyuncs.com/ecf/ubuntu:18.04.1 --platform=linux/arm64,linux/amd64 -f base.Dockerfile . --push
```

构建多平台javabase镜像
```bash
docker buildx build -t registry.cn-hangzhou.aliyuncs.com/ecf/javabase:11.1 --platform=linux/arm64,linux/amd64 . --push
```

针对arm平台单独生成tag
```bash
docker buildx build -t registry.cn-hangzhou.aliyuncs.com/ecf/javabase:11.1-arm --platform=linux/arm64 . --push
```

可以使用registry.cn-hangzhou.aliyuncs.com/ecf/javabase:11.1-arm作为基础镜像，用于jib插件构建后端服务arm架构的镜像